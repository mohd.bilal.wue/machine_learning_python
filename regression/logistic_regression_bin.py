import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import classification_report, confusion_matrix

x = np.arange(10).reshape(-1,1)
y = np.array([0, 0, 0, 0, 1, 1, 1, 1, 1, 1])

model = LogisticRegression(solver='liblinear', C=10.0, random_state=0)
model.fit(x,y)



if __name__ == "__main__":
    print(f"classes: {model.classes_}")
    print(f"intercept: {model.intercept_}")
    print(f"coefficients: {model.coef_}")
    print(f"probabilities: {model.predict_proba(x)}")
    print(f"predictions: {model.predict(x)}")
    print(f"score: {model.score(x, y)}")
    print(f"confusion matrix {confusion_matrix(y, model.predict(x))}")

    cm = confusion_matrix(y, model.predict(x))
    fig, ax = plt.subplots(figsize=(8, 8))
    ax.imshow(cm)
    ax.grid(False)
    ax.xaxis.set(ticks=(0, 1), ticklabels=('Predicted 0s', 'Predicted 1s'))
    ax.yaxis.set(ticks=(0, 1), ticklabels=('Actual 0s', 'Actual 1s'))
    ax.set_ylim(1.5, -0.5)
    for i in range(2):
        for j in range(2):
            ax.text(j, i, cm[i, j], ha='center', va='center', color='red')
    plt.show()