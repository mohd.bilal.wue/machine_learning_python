import numpy as np
from sklearn.linear_model import LinearRegression

x = np.array([5, 15, 25, 35, 45, 55]).reshape((-1, 1))
y = np.array([5, 20, 14, 32, 22, 38]).reshape((-1, 1))


model = LinearRegression(
    fit_intercept=True, normalize=False, copy_X=True, n_jobs=None)
model.fit(x, y)

r_sq = model.score(x, y)

if __name__ == "__main__":
    print(f'intercept: {model.intercept_}')
    print(f'slope: {model.coef_}')

    # predicting
    y_pred = model.predict(x)
    print(f"predicted: {y_pred}")
