import numpy as np
from sklearn.linear_model import LinearRegression

x = np.array([[0, 1], [5, 1], [15, 2], [25, 5], [35, 11], [45, 15], [55, 34], [60, 35]])
y = np.array([4, 5, 20, 14, 32, 22, 38, 43]).reshape((-1,1))

model = LinearRegression()
model.fit(x,y)

if __name__ == "__main__":
    r_sq = model.score(x, y)
    print('coefficient of determination:', r_sq)
    print('intercept:', model.intercept_)
    print('slope:', model.coef_)

    # predict
    y_pred = model.predict(x)
    print('predicted response:', y_pred, sep='\n')
