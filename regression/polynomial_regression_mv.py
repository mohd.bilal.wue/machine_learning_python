import numpy as np
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import PolynomialFeatures

x = np.array([[0, 1], [5, 1], [15, 2], [25, 5], [35, 11], [45, 15], [55, 34], [60, 35]])
y = np.array( [4, 5, 20, 14, 32, 22, 38, 43])

# add extra column x^2 to x, using PolynomialFeatures. Can also do manually
transformer = PolynomialFeatures(degree=2, include_bias=False)
transformer.fit(x)
x_ = transformer.transform(x)
model = LinearRegression().fit(x_, y)

if __name__ == "__main__":
    r_sq = model.score(x_, y)
    print('coefficient of determination:', r_sq)
    print('intercept:', model.intercept_)
    print('coefficients:', model.coef_)