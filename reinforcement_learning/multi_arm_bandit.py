import numpy as np
import random
import matplotlib.pyplot as plt


np.random.seed(5)

n = 10
arms = np.random.rand(n)
eps = 0.1

def reward(prob):
    reward = 0
    iterations = 10
    for i in range(iterations):
        if random.random()<prob:
            reward += 1
    return reward

av = np.array([np.random.randint(0,(n+1)), 0]).reshape(1,2) #av = action-value

def best_arm(a):
    best = 0
    best_mean = 0
    for u in a:
        avg = np.mean(a[np.where(a[:,0] == u[0])][:, 1])
        if best_mean < avg:
            best_mean = avg
            best = u[0]
    return best

plt.xlabel("Number of times played")
plt.ylabel("Average Reward")

for i in range(500):
    choice = None
    if random.random() > eps:
        choice = best_arm(av)
    else:
        choice = np.where(arms == np.random.choice(arms))[0][0]
    this_av = np.array([[choice, reward(arms[choice])]])
    av = np.concatenate((av, this_av), axis=0)
    running_mean = np.mean(av[:, 1])
    plt.scatter(i, running_mean)

plt.show()
