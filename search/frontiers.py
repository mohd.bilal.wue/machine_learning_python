
import abc


class IFrontier(metaclass=abc.ABCMeta):
    def add(self, node):
        pass

    def remove(self):
        pass

    def empty(self):
        pass


class Frontier(IFrontier):
    def __init__(self):
        super().__init__()
        self.frontier = []

    def add(self, node):
        self.frontier.append(node)

    def empty(self):
        return len(self.frontier) == 0

    def remove(self):
        pass

    def contains_state(self, state):
        return any(node.state == state for node in self.frontier)


class StackFrontier(Frontier):
    def __init__(self):
        super().__init__()

    def remove(self):
        if self.empty():
            raise Exception("empty frontier")
        else:
            node = self.frontier.pop()
            return node


class QueueFrontier(Frontier):
    def __init__(self):
        super().__init__()

    def remove(self):
        if self.empty():
            raise Exception("empty frontier")
        else:
            node = self.frontier.pop(0)
            return node
