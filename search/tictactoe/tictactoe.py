"""
Tic Tac Toe Player
"""

import math
import copy

X = "X"
O = "O"
EMPTY = None


def initial_state():
    """
    Returns starting state of the board.
    """
    return [[EMPTY, EMPTY, EMPTY],
            [EMPTY, EMPTY, EMPTY],
            [EMPTY, EMPTY, EMPTY]]


def player(board):
    """
    Returns player who has the next turn on a board.
    """
    x_count = sum([r.count(X) for r in board])
    o_count = sum([r.count(O) for r in board])
    if o_count < x_count:
        return O
    return X


def actions(board):
    """
    Returns set of all possible actions (i, j) available on the board.
    """
    if terminal(board):
        return set((0, 0))
    actions = set()
    for i, r in enumerate(board):
        for j, v in enumerate(r):
            if v is None:
                actions.add((i, j))
    return actions


def result(board, action):
    """
    Returns the board that results from making move (i, j) on the board.
    """
    if action not in actions(board):
        raise Exception("action not allowed")
    p = player(board)
    new_board = copy.deepcopy(board)
    i, j = action
    new_board[i][j] = O if p is O else X
    return new_board


def winner(board):
    """
    Returns the winner of the game, if there is one.
    """
    is_x = False
    is_o = False
    d1 = [board[0][0], board[1][1], board[2][2]]
    d2 = [board[0][2], board[1][1], board[2][0]]
    if all(c == X for c in d1) or all(c == X for c in d2):
        return X
    if all(c == O for c in d1) or all(c == O for c in d2):
        return O
    for r in board:
        if all(c == X for c in r):
            return X
        elif all(c == O for c in r):
            return O
    return None


def terminal(board):
    """
    Returns True if game is over, False otherwise.
    """
    if winner(board) or all(all(r) for r in board):
        return True
    return False


def utility(board):
    """
    Returns 1 if X has won the game, -1 if O has won, 0 otherwise.
    """
    if not terminal(board):
        raise Exception("game not finished")
    if winner(board) is X:
        return 1
    elif winner(board) is O:
        return -1
    else:
        return 0


def max_value(board):
    v = -1000
    res = []
    if terminal(board):
        return utility(board)
    for action in actions(board):
        v_min = min_value(result(board, action))
        v = max(v, v_min)
        res.append(action ,v)
    return v


def min_value(board):
    v = 1000
    res = []
    if terminal(board):
        return utility(board)
    for action in actions(board):
        v_max = max_value(result(board, action))
        v = min(v, v_max)
        res.append(action ,v)
    return v


def minimax(board):
    """
    Returns the optimal action for the current player on the board.
    """
    if terminal(board):
        return None

    if player(board) is X:
        v = max_value(board=board)
        print(v)
    elif player(board) is O:
        v = min_value(board=board)
        print(v)
    return v
